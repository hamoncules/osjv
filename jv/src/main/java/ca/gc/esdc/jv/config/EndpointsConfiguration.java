package ca.gc.esdc.jv.config;

import ca.gc.esdc.jv.security.BasicAuthorizationInterceptor;
import java.util.HashMap;
import java.util.Map;
import javax.xml.namespace.QName;
import org.apache.camel.component.cxf.CxfEndpoint;
import org.apache.cxf.Bus;
import org.apache.cxf.interceptor.Interceptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

@Configuration
@ImportResource({"classpath:META-INF/cxf/cxf.xml"})
public class EndpointsConfiguration {

  protected Logger logger = LoggerFactory.getLogger(EndpointsConfiguration.class);

  @Autowired
  private ApplicationContext applicationContext;

  @Autowired
  private Bus bus;

  @Value("${inbound.jv.basicauth.enabled}")
  private Boolean inboundJvBasicAuthEnabled;

  @Value("${inbound.jv.basicauth.usernames}")
  private String inboundJvBasicAuthUsernames;

  @Value("${inbound.jv.basicauth.passwords}")
  private String inboundJvBasicAuthPasswords;

  @Value("${inbound.jv.wsdl.namespace}")
  private String inboundJvWsdlNamespace;

  @Value("${inbound.jv.wsdl.service.name}")
  private String inboundJvWsdlServiceName;

  @Value("${inbound.jv.wsdl.port.name}")
  private String inboundJvWsdlPortName;

  /**
   * Returns the interceptor for the Basic Authorization security scheme.
   *
   * @return the security interceptor
   */
  @Bean(name = "securityInterceptor")
  public BasicAuthorizationInterceptor getSecurityInterceptor() {
    Map<String, String> users = new HashMap<String, String>();
    String[] usernames = inboundJvBasicAuthUsernames.split(",");
    String[] passwords = inboundJvBasicAuthPasswords.split(",");
    for (int i = 0; i < usernames.length; i++) {
      users.put(usernames[i].trim(), passwords[i].trim());
    }
    BasicAuthorizationInterceptor interceptor = new BasicAuthorizationInterceptor(users);
    return interceptor;
  }

  /**
   * Returns the CXF endpoint configured with settings from the application properties file.
   *
   * @return the CXF endpoint
   */
  @Bean(name = "jvInbound")
  public CxfEndpoint jvInbound() {
    CxfEndpoint cxfEndpoint = new CxfEndpoint();
    cxfEndpoint.setBeanId("jvInbound");
    cxfEndpoint.setAddress("{{inbound.jv.scheme}}"
        + "://"
        + "{{inbound.jv.host}}"
        + ":"
        + "{{inbound.jv.port}}"
        + "/"
        + "{{inbound.jv.resource.path}}");
    cxfEndpoint.setPublishedEndpointUrl("{{inbound.jv.frontend.protocol}}"
        + "://{{inbound.jv.frontend.host}}"
        + ":"
        + "{{inbound.jv.frontend.port}}"
        + "/"
        + "{{inbound.jv.frontend.context.prefix}}"
        + "{{inbound.jv.resource.path}}");
    cxfEndpoint.setWsdlURL("{{inbound.jv.wsdl.location}}");
    cxfEndpoint.setBus(bus);
    cxfEndpoint.setServiceNameAsQName(
        new QName(inboundJvWsdlNamespace, inboundJvWsdlServiceName, "s"));
    cxfEndpoint.setEndpointNameAsQName(
        new QName(inboundJvWsdlNamespace, inboundJvWsdlPortName, "s"));
    if (inboundJvBasicAuthEnabled) {
      cxfEndpoint.getInInterceptors()
          .add((Interceptor) applicationContext.getBean("securityInterceptor"));
    }
    return cxfEndpoint;
  }

}
