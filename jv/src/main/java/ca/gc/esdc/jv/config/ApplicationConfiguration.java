package ca.gc.esdc.jv.config;

import org.apache.camel.CamelContext;
import org.apache.camel.component.micrometer.eventnotifier.MicrometerExchangeEventNotifier;
import org.apache.camel.component.micrometer.messagehistory.MicrometerMessageHistoryFactory;
import org.apache.camel.component.micrometer.routepolicy.MicrometerRoutePolicyFactory;
import org.apache.camel.spring.boot.CamelContextConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ApplicationConfiguration {

  protected Logger logger = LoggerFactory.getLogger(ApplicationConfiguration.class);


  @Bean
  CamelContextConfiguration contextConfiguration() {
    return new CamelContextConfiguration() {
      @Override
      public void afterApplicationStart(CamelContext camelContext) {
        logger.info("Components:\n");
        for (String name : camelContext.getComponentNames()) {
          logger.info(name);
        }

        logger.info("Configuration is complete.");
      }

      @Override
      public void beforeApplicationStart(CamelContext context) {
        context.setStreamCaching(true);
        context.addRoutePolicyFactory(new MicrometerRoutePolicyFactory());
        context.setMessageHistoryFactory(new MicrometerMessageHistoryFactory());
        context.getManagementStrategy().addEventNotifier(new MicrometerExchangeEventNotifier());
      }
    };
  }

}
