package ca.gc.esdc.jv.route;

import org.apache.camel.LoggingLevel;
import org.apache.camel.builder.RouteBuilder;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;

@Component
public class InboundRoutes extends RouteBuilder {

  protected Logger logger = LoggerFactory.getLogger(InboundRoutes.class);

  @Override
  public void configure() throws Exception {

    from("cxf:bean:jvInbound?dataFormat={{cxf.dataformat}}")
        .routeId("jvInbound")
        .log(LoggingLevel.INFO, "request.operation: ${in.header.operationName}")
        .log(LoggingLevel.INFO, "request.headers: ${in.headers}")
        .log(LoggingLevel.INFO, "request.body: ${body}")
        .choice()
        .when(simple("${in.header.operationName} == 'JournalVoucherCreateRequest'"))
        .to("direct:jvOutbound")
        .otherwise()
        .log(LoggingLevel.ERROR, "Operation not available.")
        .end()
        .log(LoggingLevel.INFO, "response.body: ${body}");

    from("direct:jvOutbound")
        .routeId("jvOutbound")
        .log(LoggingLevel.INFO, "Validating incoming payload.")
        .to("validator:xsd/Finance.xsd")
        // TODO: route to a JMS queue
        // TODO: remove stubbed response
        .to("stub:jvResponse");

    from("stub:jvResponse")
        .routeId("jvResponse")
        .log(LoggingLevel.INFO, "Responding with stub payload.")
        .setBody(simple(IOUtils.toString(
            new ClassPathResource("xml/JournalVoucherCreateConfirmation.xml")
                .getInputStream(), "UTF-8")));

  }
}
